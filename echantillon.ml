open Geometry

let lerp t a b = (1.0 -. t) *. a +. t *. b

let cone ctm =
    let u1 = Random.float 1.0 in
    let u2 = Random.float 1.0 in
    let ct = lerp u1 ctm 1.0 in
    let st = sqrt (1.0 -. ct *. ct) in
    let phi = u2 *. 2.0 *. pi in
    { x = cos phi *. st; y = sin phi *. st; z = ct }

let hemisphere () =
    let u1 = Random.float 1.0 in
    let u2 = Random.float 1.0 in
    let z = u1 in
    let r = sqrt(max 0.0 (1.0 -. z*.z)) in
    let phi = 2.0 *. pi *. u2 in
    let x = r *. cos phi in 
    let y = r *. sin phi in
    { x = x; y = y; z = z }

let sphere () =
    let u1 = Random.float 1.0 in
    let u2 = Random.float 1.0 in
    let z = 1.0 -. 2.0 *. u1 in
    let r = sqrt(max 0.0 (1.0 -. z*.z)) in
    let phi = 2.0 *. pi *. u2 in
    let x = r *. cos phi in 
    let y = r *. sin phi in
    { x = x; y = y; z = z }

let disque () =
    let u1 = Random.float 1.0 in
    let u2 = Random.float 1.0 in
    let r = sqrt u1 in
    let theta = 2.0 *. pi *. u2 in
    { x = r *. cos theta; y = r *. sin theta; z = 0.0 }
    
let hemisphere_cos () =
    let v = disque () in
    { x = v.x; y = v.y; z = sqrt (max 0.0 (1.0 -. v.x *. v.x -. v.y *. v.y)) }

let hemisphere_pdf v = 1.0 /. (2.0 *. pi)
let hemisphere_cos_pdf v = v.z /. pi

let weight a b = (a *. a) /. (a *. a +. b *. b)
