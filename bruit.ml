open Geometry

(* Retourne un flottant dans [-1,1]
 * suivant un motif pseudo-aleatoire *)
let bruit1d x =
    let decx = (x lsl 13) lxor x in
    1.0 -. 
        (float_of_int 
            (abs ((decx * (decx * decx * 15731 + 789221) + 1299553) land
            0x7fffffff))
        /. 1073741824.0)

let bruit2d x y = bruit1d (x + 57 * y)
let bruit3d x y z = bruit1d (x + 57 * y + 57 * 57 * z)

(* Linear interpolation *)
let lerp t a b = (1.0 -. t) *. a +. t *. b
(* Scurve *)
let scurve t = t *. t *. (3.0 -. 2.0 *. t)

let perlin2d x y =
    let x = x +. 1.0e7 in
    let y = y +. 1.0e7 in

    let ix = int_of_float x in
    let iy = int_of_float y in

    (* ul -- ur
     *  |     |
     * ll -- lr (upper,lower, left,right)
     *)

    let ul = bruit2d ix iy in
    let ur = bruit2d (ix+1) iy in
    let ll = bruit2d ix (iy+1) in
    let lr = bruit2d (ix+1) (iy+1) in

    let tx = scurve (x -. float_of_int ix) in
    let ty = scurve (y -. float_of_int iy) in

    let tu = lerp tx ul ur in
    let tl = lerp tx ll lr in

    lerp ty tu tl

let perlin3d v =
    let x = v.x +. 1.0e7 in
    let y = v.y +. 1.0e7 in
    let z = v.z +. 1.0e7 in

    let ix = int_of_float x in
    let iy = int_of_float y in
    let iz = int_of_float z in

    let ful = bruit3d ix iy iz in
    let fur = bruit3d (ix+1) iy iz in
    let fll = bruit3d ix (iy+1) iz in
    let flr = bruit3d (ix+1) (iy+1) iz in

    let bul = bruit3d ix iy (iz+1) in
    let bur = bruit3d (ix+1) iy (iz+1) in
    let bll = bruit3d ix (iy+1) (iz+1) in
    let blr = bruit3d (ix+1) (iy+1) (iz+1) in

    let tx = scurve (x -. float_of_int ix) in
    let ty = scurve (y -. float_of_int iy) in
    let tz = scurve (z -. float_of_int iz) in

    let tfu = lerp tx ful fur in
    let tfl = lerp tx fll flr in

    let tf = lerp ty tfu tfl in

    let tbu = lerp tx bul bur in
    let tbl = lerp tx bll blr in

    let tb = lerp ty tbu tbl in

    lerp tz tf tb

let bois i lowfreq freqmult = 
    let basefreq = ref lowfreq in
    let amp = ref 0.5 in
    let b = ref 0.0 in
    for loop = 0 to 10 do
        b := !b +. !amp *. fabs (perlin3d (!basefreq *% i));
        basefreq := !basefreq *. freqmult;
        amp := !amp *. 0.5
    done;
    let b01 = (1.0 +. !b)*.0.5 in
    b01

let nuage i lowfreq freqmult cover sharpness = 
    let basefreq = ref lowfreq in
    let amp = ref 0.5 in
    let b = ref 0.0 in
    for loop = 0 to 5 do
        b := !b +. !amp *. perlin3d (!basefreq *% i);
        basefreq := !basefreq *. freqmult;
        amp := !amp *. 0.5
    done;
    let bc = !b -. cover in
    let c = if bc < 0.0
            then 0.0
            else bc in
    1.0 -. (sharpness ** c)
