type t = {
    x : float;
    y : float;
    z : float;
}

let normalize v =
    let n = sqrt(v.x *. v.x +. v.y *. v.y +. v.z *. v.z) in
    if n > 0.0
    then { x=v.x/.n; y=v.y/.n; z=v.z/.n }
    else { x=1.0; y=0.0; z=0.0 }

let ( +-+ ) v1 v2 = {
    x = v1.x +. v2.x ;
    y = v1.y +. v2.y ;
    z = v1.z +. v2.z
    }
