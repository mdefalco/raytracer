open Geometry

type t = {
    p1 : float3;
    p2 : float3;
    p3 : float3;
    n1 : float3;
    n2 : float3;
    n3 : float3
    }

let aabb triangle =
    let p1 = triangle.p1 in
    let p2 = triangle.p2 in
    let p3 = triangle.p3 in
    {
        Aabb.xlow = min p1.x (min p2.x p3.x);
        Aabb.xhigh = max p1.x (max p2.x p3.x);
        Aabb.ylow = min p1.y (min p2.y p3.y);
        Aabb.yhigh = max p1.y (max p2.y p3.y);
        Aabb.zlow = min p1.z (min p2.z p3.z);
        Aabb.zhigh = max p1.z (max p2.z p3.z)
    }

(* m est dans le triangle, c'est donc un barycentre 
 * des trois points 
 * mx = a p1x + b p1y + c p1z
 * my = a p1y + b p1y + c p1z
 * mz = a p1z + b p1y + c p1z
 * *)
let normale triangle m =
    let p1, p2, p3 = triangle.p1, triangle.p2, triangle.p3 in
    let n1, n2, n3 = triangle.n1, triangle.n2, triangle.n3 in
    let a, b, c = Solver.syslin3 
        [| [| p1.x; p2.x; p3.x |];
           [| p1.y; p2.y; p3.y |];
           [| p1.z; p2.z; p3.z |] |]
        [| m.x; m.y; m.z |] in
    a *% n1 +$ b *% n2 +$ c *% n3

let intersection ray triangle =
    let p1, p2, p3 = triangle.p1, triangle.p2, triangle.p3 in
    let plan = {
        Plan.origine = triangle.p1;
        Plan.normale = normalize ( (p2 -$ p1) ^$ (p3 -$ p1) )
    } in
    let i = Plan.intersection ray plan in
    match i with
    None -> None
    | Some t -> let i = Ray.impact ray t in
        let ir = i -$ ray.Ray.source in
        let dansface a b =
            let va = a -$ ray.Ray.source in
            let vb = b -$ ray.Ray.source in
            let nf = vb ^$ va in
            ir *|* nf >= 0.0 in
        if dansface triangle.p1 triangle.p2
            && dansface triangle.p2 triangle.p3
            && dansface triangle.p3 triangle.p1
        then Some t
        else None

let texcoords sph m = m
