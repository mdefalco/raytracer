open Geometry
open Objet
open Primitive
open Matiere
open Sphere


let pi = 3.1415926535 

let couleur_id id =
    Random.init id;
    { x = Random.float 1.0; y = Random.float 1.0; z = Random.float 1.0 }

let absf x = if x<0. then -.x else x

(* let aleat1 () = 
let a = ref 0 in
while !a=0 do a:= Random.int 3161 done; 
let b = ref (1000./.float_of_int (!a) ) in
while !b > 1. do b:= !b/.10. done; !b;; *)
			  
let lumisph = { primitive = 
	Sphere {
    centre = {x=250.;y=500.;z=300.};
    rayon = 50.};
    matiere = {
        diffuse= Constante zero3;
        ambiante = {x=0.9;y=0.9;z=0.9};
        speculaire = zero3,0.;
        reflectivite = 1.;
        transparence = 0.
	}
} 
(*let lumisph = { primitive = 
	Sphere {
    centre = {x=50.;y=81.6-.16.5;z=81.6};
    rayon = 7.};
    matiere ={
    diffuse= Constante zero3;
	ambiante = {x=400.;y=400.;z=400.};
	speculaire = zero3,0.;
    reflectivite = 1.;
	transparence = 0.
		    }
}*)

let cree_base w =
			let w = normalize w in
			let u = if (absf w.x) > 0. then normalize ({x=0.;y=1.;z=0.}^$w) else normalize({x=1.;y=1.;z=1.} ^$ w) in (*vecteur perpendiculaire � w *)
			let v = w ^$ u in (*vecteur perpendiculaire � w et u *)
			u,v,w
			
let aleatsphere u1 u2  =
			let zz = 1.-.2.*.u1 in
			let r = sqrt( max 0. (1.-.zz*.zz)) in
			let phi = 2.*.pi*.u2 in
			let xx = r*.cos(phi) in
			let yy = r*.sin(phi) in
			{x=xx;y=yy;z=zz}
			
let aleathemisphere n = 
			let r1 = 2.*.pi*.((Random.float 1.0)) in (*angle al�atoire dans (O,x,y)*)
			let r2 = Random.float 1.0 in
			let r2s = sqrt(r2) in (*distance al�atoire du centre *)
			let u,v,w = cree_base n in
			let d = normalize ((r2s*. cos(r1)) *% u +$ (r2s*. sin(r1)) *% v +$ (sqrt(1.-.r2)) *% w) in (*direction dans laquelle on renvoie al�atoirement le rayon*)
			d

let ombre  ray t lumisph i =
			let Sphere {centre=c;rayon = r} = lumisph.primitive in (*la source lumineuse*)
			let i = Ray.impact ray (t*.0.99) in
            let impa = i in
			let su,sv,sw = cree_base (c-$ i) in
           	let cos_a_max = sqrt( 1.-.r*.r/.((i-$c)*|*(i-$c))) in (*Monte carlo ray tracing p33*)
			let eps1 = Random.float 1.0 in
			let eps2 = Random.float 1.0 in
			let cos_a = 1.-.eps1 +. eps1 *.cos_a_max in
			let sin_a = sqrt(1.-.cos_a*.cos_a) in
			let phi = 2.*.pi*.eps2 in
			let l =  ( (cos(phi)*.sin_a) *% su +$ (sin(phi)*.sin_a) *% sv +$ (cos_a) *% sw ) in
			let len = sqrt((l-$i)*|*(l-$i)) in
			let dirsha = normalize (l(*-$impa*)) in
	        let shadow_ray = Ray.relance ray (impa) (dirsha) in
			shadow_ray,l,cos_a_max
			
