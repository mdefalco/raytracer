type expr =
      EFloat3 of expr * expr * expr
    | ConstantFloat of float
    | ConstantInt of int
    | Id of string
    | Norme of expr
    | Sqrt of expr
    | Cos of expr
    | Sin of expr
    | Tan of expr
    | Exp of expr
    | Add of expr * expr
    | Mult of expr * expr
    | Div of expr * expr
    | Moins of expr * expr
    | Pow of expr * expr
    | Oppose of expr

type matiere_fonction =
    Echiquier of expr * expr * expr
    | Bois of expr * expr * expr * expr
    
type matiere_element =
      Diffuse of expr
    | DiffuseFunc of matiere_fonction
    | Speculaire of expr * expr
    | Ambiante of expr
    | Reflectivite of expr
    | Transparence of expr

type option_item = ObjetID | NormaleMap | Bvh of int

type camera_item =
    CIOrigine of expr
    | CIRegarde of expr
    | CIDirection of expr
    | CIHaut of expr
    | CIChampDeVision of expr
    | CIAspect of expr
    | CILargeur of expr
    | CIPixels of expr
    | CIAnticrenelage of expr

type matiere =
    MatiereNommee of string
    | MatiereDeclaree of matiere_element list

type t = 
    Assign of string * expr
    | AssignMatiere of string * matiere_element list
    | For of string * expr * expr * t list
    | Sphere of expr * expr * matiere option
    | Plan of expr * expr * matiere option
    | Triangle of expr list * expr list option * matiere option
    | ChampDeDistance of expr * expr * expr * matiere option
    | Mandelbulb of expr * expr * expr * matiere option
    | Implicite of expr * expr * expr * matiere option
    | Mesh of expr * expr * expr * expr * expr * expr * matiere option
    | Lumiere of expr * expr * expr
    | Camera of camera_item list
    | Options of option_item list
    | Aabb of float * float * float * float * float * float
