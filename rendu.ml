open Geometry
open Objet

let couleur_id id =
    Random.init id;
    { x = Random.float 1.0; y = Random.float 1.0; z = Random.float 1.0 }

type debug_rayon = Impact of float3 * float3 | Direction of float3 * float3
let debug_path = ref []

let rec trace_chemin ray scene couleur filtre debug =
    let i = Scene.intersection ray scene in
    match i with
    | Vide -> couleur 
    | Point (t, obj) -> 
        let i = Ray.impact ray t in
        if debug then debug_path := Impact (ray.Ray.source, i)::!debug_path;

        let ip = Ray.impact ray (t*.0.999) in
        let n = Objet.normale obj i in
        let texcoords = Objet.texcoords obj i in
        
        (* Primitives double face *)
        (*
        let d = ray.Ray.direction *|* n in
        let n = if d > 0.0 then -1.0 *% n else n in
    *)

        let mat = obj.Objet.matiere in

        (* 
        if scene.Scene.options.Scene.objet_id
        then couleur_id (Scene.objet_id scene obj)
        else if scene.Scene.options.Scene.normale_map
        then n
        else begin
        *)

        (*let couleur = ref mat.Matiere.ambiante in*)

        (* for lumi = 0 to Array.length scene.Scene.lumieres - 1 do *)
        let lumi = Random.int (Array.length scene.Scene.lumieres) in
        let lum = scene.Scene.lumieres.(lumi) in
        let li, u, lum_pdf = Lumiere.echantillonne lum i in
        let il = li -$ i in
        let dil = il *|* il in
        let ray_ombre = Ray.relance ray ip u in

        if debug then debug_path := Direction (ip, u)::!debug_path;
        let ombre = match Scene.intersection ray_ombre scene with
        | Vide -> false
        | Point(_,{ primitive=Primitive.Lumiere l; matiere=_ }) when l = lum -> false
        | Point (t,_) when t*.t > dil -> false 
        | _ -> true in
           
        let dif = (1.0 /. pi) *% Matiere.evalue mat.Matiere.diffuse texcoords in

        let ldif = ref zero3 in

        if not ombre && lum_pdf > 0.0
        then begin
            let psnu = n *|* u in
            let bsdf_pdf = psnu /. pi in
            let w = Echantillon.weight lum_pdf bsdf_pdf in
            let lum_contrib = (w *. fabs psnu /. lum_pdf) *% (dif *$ lum.Lumiere.couleur) in
            if debug then Printf.printf "lum %s\n" (string_of_float3 lum_contrib);
            ldif := !ldif +$ lum_contrib;
            let dir_dif = comblin (Echantillon.hemisphere_cos ()) (cree_base n) in
            if debug then debug_path := Direction (i, dir_dif)::!debug_path;
            match Scene.intersection (Ray.aux ip dir_dif) scene with
            | Point(_,{ primitive=Primitive.Lumiere l; matiere=_ }) when l = lum -> begin
                let psnd = n *|* dir_dif in
                let bsdf_pdf = psnd /. pi in
                let w = Echantillon.weight bsdf_pdf lum_pdf in
                let bsdf_contrib = (w *. fabs psnd /. bsdf_pdf) *% (dif *$ lum.Lumiere.couleur) in
                if debug then Printf.printf "bsdf %s\n" (string_of_float3 bsdf_contrib);
                ldif := !ldif +$ bsdf_contrib
            end
            | _ -> ()
        end;

        let probCont = 0.5 in
        let cut = ref false in
        let nouveau_filtre = ref filtre in

        if ray.Ray.rebonds >= 2 
        then begin
            cut := Random.float 1.0 > probCont;
            nouveau_filtre := (1.0 /. probCont) *% (!nouveau_filtre)
        end;

        if ray.Ray.rebonds > 10
        then cut := true;

        if debug
        then Printf.printf "%s + %s * %s = %s\n"
            (string_of_float3 couleur)
            (string_of_float3 !ldif)
            (string_of_float3 filtre)
            (string_of_float3 (couleur +$ (!ldif *$ filtre)));

        if !cut
        then couleur +$ (!ldif *$ filtre)
        else begin
            let u = Random.float 1.0 in
            let dir_rad = 
                (*if u < mat.Matiere.reflectivite 
                then Ray.reflexion (-1.0 *% ray.Ray.direction) n
                else*)
                comblin (Echantillon.hemisphere_cos ()) (cree_base n)
            in
            let ray_rad = Ray.relance ray ip dir_rad in
            if debug then debug_path := Direction (i, dir_rad)::!debug_path;
            let psnr = dir_rad *|* n in
            let pdf = psnr /. pi in
            nouveau_filtre := (fabs psnr /. pdf) *% (dif *$ !nouveau_filtre);
            trace_chemin ray_rad scene (couleur +$ (!ldif *$ filtre)) (!nouveau_filtre) debug
        end

let debug_point = (110,110)

let couleur_rayon ray scene = 
    let debug = if ray.Ray.coord_ecran = debug_point
    then (debug_path:= []; true)
    else false  in
    let {x = x; y = y; z = z} =  trace_chemin ray scene zero3 un3 debug in
    let clamp v = max 0.0 (min 1.0 v) in
    {x = clamp x; y = clamp y; z = clamp z}

let optique_inv cam w h v =
    let pel = Pellicule.cree cam in
    let o = cam.Camera.origine in
    let po = pel.Pellicule.coin_inferieur_gauche in
    let d = normalize (v -$ o) in
    let r = Ray.aux o d in
    let pel_plan = { Plan.origine = po; Plan.normale = cam.Camera.direction } in
    let largeur = cam.Camera.largeur in
    let hauteur = cam.Camera.aspect *. largeur in
    match Plan.intersection r pel_plan with
    | None -> raise Not_found
    | Some t -> let m = o +$ (t *% d) in
        let mx = int_of_float 
            (float_of_int w *. (pel.Pellicule.vec_largeur *|* (m -$ po))/.
            (largeur*. largeur)) in
        let my = int_of_float
            (float_of_int h *. (pel.Pellicule.vec_hauteur *|* (m -$ po))/.
            (hauteur *. hauteur)) in
        if mx >= 0 && mx < w && my >= 0 && my < h
        then (mx, my)
        else raise Not_found (*begin
            if mx <0 then (0,h / 2)
            else if mx >= w then (w-1,h/2)
            else if my < 0 then (w/2, 0) 
            else (w/2,h-1)
        end*)


