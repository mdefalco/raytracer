
(* regle de sarrus *)
let det3 a = a.(0).(0) *. a.(1).(1) *. a.(2).(2)
    +. a.(0).(2) *. a.(1).(0) *. a.(2).(1)
    +. a.(0).(1) *. a.(1).(2) *. a.(2).(0)
    -. a.(0).(1) *. a.(1).(0) *. a.(2).(2)
    -. a.(0).(2) *. a.(1).(1) *. a.(2).(0)
    -. a.(0).(0) *. a.(1).(2) *. a.(2).(1)

let copy_matrix a =
    let na = Array.init (Array.length a)
        (fun i -> Array.copy a.(i)) in
    na

let subscol a b j =
    let na = copy_matrix a in
    for i = 0 to Array.length a - 1 do
        na.(i).(j) <- b.(i)
    done;
    na

(* formules de cramer *)
let syslin3 a b =
    let da = det3 a in
    ( det3 (subscol a b 0) /. da,
      det3 (subscol a b 1) /. da,
      det3 (subscol a b 2) /. da )

