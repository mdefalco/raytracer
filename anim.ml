open Geometry

let calcule scene fo t = 
    let cam = scene.Scene.camera in
    let w = cam.Camera.pixels in
    set_echelle_pixel w;
    let h = int_of_float ((float_of_int w)*. cam.Camera.aspect) in
    let f = open_out fo in
    Printf.fprintf f "P6\n%d %d\n255\n" w h;
    for j = 0 to h-1 do
        let j = h-1-j in
     for i = 0 to w-1 do
         let ray = Ray.optique cam w h i j 0.0 0.0 in
         let coul = ref (Rendu.couleur_rayon ray scene) in
         (* Anti-aliasing *)
        let nsamples = 1 +cam.Camera.anticrenelage in
        for s = 1 to nsamples-1 do
            let dx = Random.float 1.0 -. 0.5 in
            let dy = Random.float 1.0 -. 0.5 in
            let ray_aa = Ray.optique cam w h i j dx dy in
            coul := !coul +$ Rendu.couleur_rayon ray_aa scene
        done;
        coul := (1.0 /. float_of_int nsamples) *% !coul;
         let convert f = max 0 (min 255 (int_of_float (255. *. f))) in
         let couleur_rayon r g b = output_char f (char_of_int r); 
            output_char f (char_of_int g);
            output_char f (char_of_int b) in
         couleur_rayon
            (convert !coul.x) (convert !coul.y) (convert !coul.z)
     done;
     Printf.printf "[%f] Calcul %f%%.\n" t (100.0 -. 100.0 *. float_of_int j /. float_of_int h);
     flush stdout
    done;
    close_out f

let load_file f =
    let ic = open_in f in
    let n = in_channel_length ic in
    let s = String.create n in
    really_input ic s 0 n;
    close_in ic;
    s

let _ =
    let s = load_file Sys.argv.(1) in
    let n = int_of_string Sys.argv.(3) in
    for i = 0 to (n+1) do
        let t = float_of_int i /. float_of_int n in
        calcule (Sceneeval.from_string s t) 
            (Printf.sprintf "%s-%03d.ppm" Sys.argv.(2) i) t
    done
