type t = {
    primitive : Primitive.t;
    matiere : Matiere.t
    }

type intersection = Vide | Point of float * t

let min_inter i1 i2 =
    match i1,i2 with
      Vide, Vide -> Vide
    | Vide, _ -> i2
    | _, Vide -> i1
    | Point (t1,_), Point (t2,_) -> if t1 < t2 then i1 else i2

let intersection ray o =
    match Primitive.intersection ray o.primitive with
      None -> Vide
    | Some t -> Point (t, o)

let normale o = Primitive.normale o.primitive
let texcoords o = Primitive.texcoords o.primitive
