open Geometry

type couleur = 
    Constante of float3
    | Echiquier of float3 * float3 * float
    | Bois of float3 * float3 * float * float

type t = {
    diffuse : couleur;
    ambiante : float3;
    speculaire : float3 * float;
    reflectivite : float;
    transparence : float;
}

let evalue couleur t =
    match couleur with
    Constante c -> c
    | Echiquier (c1,c2,a) ->
        let v = a *% t in
        let vu = int_of_float v.x in
        let vv = int_of_float v.z in
        if (vu + vv) mod 2 = 0
        then c1 else c2
    | Bois (c1,c2,a,f) ->
        let v = f *% t in
        let r = a *. sqrt(v.x*. v.x +. v.z *. v.z) in
        let t = 0.5 *. (sin r +. 1.0) in
        t *% c1 +$ (1.0 -. t) *% c2

let diffuse c = {
    diffuse = Constante c;
    ambiante = { x = 0.1; y = 0.1; z = 0.0 };
    speculaire = ({ x = 1.0; y = 1.0; z = 1.0 }, 100.0);
    reflectivite = 0.0;
    transparence = 0.0
}

let aleatoire () = {
    diffuse = Constante { x = Random.float 1.0; y = Random.float 1.0; z = Random.float 1.0 };
    ambiante = { x = 0.1; y = 0.1; z = 0.0 };
    speculaire = ({ x = 1.0; y = 1.0; z = 1.0 }, 100.0);
    reflectivite = 0.0;
    transparence = 0.0
}

