%token <int> INT
%token <string> ID
%token <float> FLOAT
%token UMINUS
%token PLUS MINUS TIMES DIV POW PIPE SQRT
%token LPAREN RPAREN LACCO RACCO COMMA
%token FOR TO LET EQUALS
%token DO DONE SPHERE CENTRE RAYON MATIERE DIFFUSE SPECULAIRE
%token TRANSPARENCE REFLECTIVITE AMBIANTE
%token LUMIERE COULEUR ORIGINE NORMALE PLAN
%token TRIANGLE POINTS COS SIN TAN PI CHAMPDEDISTANCE
%token OPTIONS OBJETID BVH
%token NORMALES TEXCOORDS CAMERA ANTICRENELAGE LARGEUR DOMAINE IMPLICITE
%token REGARDE HAUT CHAMPDEVISION ASPECT PIXELS DIRECTION AABB MANDELBULB
%token EOF

%left PLUS MINUS        /* lowest precedence */
%left TIMES DIV         /* medium precedence */
%left POW
%nonassoc UMINUS        /* highest precedence */


%{
open Abstractscene
%}

%start <Abstractscene.t list> main
%%

main:
| e = nonempty_list(stmt) EOF { e }

stmt:
| LET i = ID EQUALS v = expr { Assign(i,v) }
| MATIERE i = ID EQUALS 
    LACCO ml = list(matiere_element) RACCO
    { AssignMatiere(i, ml) }
| FOR i = ID EQUALS is = expr TO ie = expr DO
    fl = nonempty_list(stmt)
  DONE  { For(i,is,ie,fl) }
| LUMIERE e = expr COULEUR c = expr { Lumiere(e,ConstantFloat 1.0,c) }
| SPHERE LACCO 
    CENTRE c = expr
    RAYON r = expr
    m = ioption(matiere_decl)
  RACCO { Sphere(c,r,m) }
| PLAN LACCO
    ORIGINE o = expr
    NORMALE n = expr
    m = ioption(matiere_decl)
  RACCO { Plan(o,n,m) }
| AABB LACCO
    xlow = FLOAT xhigh = FLOAT
    ylow = FLOAT yhigh = FLOAT
    zlow = FLOAT zhigh = FLOAT
  RACCO  { Aabb (xlow,xhigh,ylow,yhigh,zlow,zhigh) }
| TRIANGLE LACCO
    POINTS p = expr_list
    n = ioption(normales) 
    t = ioption(texcoords) 
    m = ioption(matiere_decl)
  RACCO { Triangle(p,n,m) }
| CAMERA LACCO
    cil = list(camera_item)
  RACCO { Camera cil }
| IMPLICITE LACCO
    f = expr
    DOMAINE low = expr COMMA high = expr
    m = ioption(matiere_decl)
  RACCO { Implicite (f,low,high,m) }
| CHAMPDEDISTANCE LACCO
    f = expr
    DOMAINE low = expr COMMA high = expr
    m = ioption(matiere_decl)
  RACCO { ChampDeDistance (f,low,high,m) }
| MANDELBULB LACCO
    puiss = expr
    DOMAINE low = expr COMMA high = expr
    m = ioption(matiere_decl)
  RACCO { Mandelbulb (puiss,low,high,m) }
| OPTIONS LACCO 
    oil = list(option_item)
 RACCO { Options oil }
    
option_item:
| OBJETID { ObjetID }
| NORMALE { NormaleMap }
| BVH level = INT { Bvh level }

camera_item:
| ORIGINE e = expr { CIOrigine e }
| REGARDE e = expr { CIRegarde e }
| DIRECTION e = expr { CIDirection e }
| HAUT e = expr { CIHaut e }
| CHAMPDEVISION e = expr { CIChampDeVision e }
| ASPECT e = expr { CIAspect e }
| PIXELS e = expr { CIPixels e }
| LARGEUR e = expr { CILargeur e }
| ANTICRENELAGE e = expr { CIAnticrenelage e }

matiere_decl:
| MATIERE m = ID { MatiereNommee m }
| MATIERE LACCO ml = list(matiere_element) RACCO { MatiereDeclaree ml }

texcoords:
| TEXCOORDS tl = expr_list { tl }

normales:
| NORMALES nl = expr_list { nl }

expr_list:
| l = separated_nonempty_list(COMMA, expr) { l }

expr:
| e1 = expr PLUS e2 = expr { Add(e1,e2) }
| e1 = expr TIMES e2 = expr { Mult(e1,e2) }
| e1 = expr DIV e2 = expr { Div(e1,e2) }
| e1 = expr MINUS e2 = expr { Moins(e1,e2) }
| e1 = expr POW e2 = expr { Pow(e1,e2) }
| LPAREN e = expr RPAREN { e }
| MINUS e = expr %prec UMINUS { Oppose e }
| LPAREN x = expr COMMA y = expr COMMA z = expr RPAREN { EFloat3 (x,y,z) }
| id = ID { Id id }
| i = INT { ConstantInt i }
| f = FLOAT { ConstantFloat f }
| PIPE PIPE e = expr PIPE PIPE { Norme e }
| PI { ConstantFloat 3.1415 }
| SQRT LPAREN e = expr RPAREN { Sqrt e }
| COS LPAREN e = expr RPAREN { Cos e }
| SIN LPAREN e = expr RPAREN { Sin e }
| TAN LPAREN e = expr RPAREN { Tan e }

matiere_element:
| DIFFUSE d = expr { Diffuse d }
| SPECULAIRE s = expr COMMA k = expr { Speculaire (s,k) }
| AMBIANTE a = expr { Ambiante a }
| REFLECTIVITE r = expr { Reflectivite r }
| TRANSPARENCE t = expr { Transparence t }

