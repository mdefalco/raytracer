type t = Sphere of Sphere.t
    | Plan of Plan.t
    | Triangle of Triangle.t
    | Implicite of Implicite.t
    | ChampDeDistance of Champdedistance.t
    | Aabb of Aabb.t
    | Mesh of Mesh.t
    | Lumiere of Lumiere.lumiere

let aabb p = 
    match p with
        Sphere sph -> Some (Sphere.aabb sph)
        | Plan plan -> None
        | Triangle triangle -> Some (Triangle.aabb triangle)
        | Implicite imp -> Some (Implicite.aabb imp)
        | ChampDeDistance imp -> Some (Champdedistance.aabb imp)
        | Aabb aabb -> Some aabb
        | Lumiere lum -> Some (Sphere.aabb (Lumiere.to_sphere lum))
        | Mesh m -> Some (Mesh.aabb m)

let intersection ray p = 
    match p with
        Sphere sph -> Sphere.intersection ray sph
        | Plan plan -> Plan.intersection ray plan
        | Triangle triangle -> Triangle.intersection ray triangle
        | Implicite imp -> Implicite.intersection ray imp
        | ChampDeDistance imp -> Champdedistance.intersection ray imp
        | Aabb aabb -> Aabb.intersection ray aabb
        | Mesh m -> Mesh.intersection ray m
        | Lumiere lum -> Sphere.intersection ray (Lumiere.to_sphere lum)

let normale p =
    match p with
    Sphere sph -> Sphere.normale sph
    | Plan plan -> Plan.normale plan
    | Triangle triangle -> Triangle.normale triangle
    | Implicite imp -> Implicite.normale imp
    | ChampDeDistance imp -> Champdedistance.normale imp
    | Aabb aabb -> Aabb.normale aabb
    | Mesh m -> Mesh.normale m
    | Lumiere lum -> Sphere.normale (Lumiere.to_sphere lum)

let texcoords p =
    match p with
    Sphere sph -> Sphere.texcoords sph
    | Plan plan -> Plan.texcoords plan
    | Triangle triangle -> Triangle.texcoords triangle
    | Implicite imp -> Implicite.texcoords imp
    | ChampDeDistance imp -> Champdedistance.texcoords imp
    | Aabb aabb -> Aabb.texcoords aabb
    | Mesh m -> Mesh.texcoords m
    | Lumiere lum -> Sphere.texcoords (Lumiere.to_sphere lum)

