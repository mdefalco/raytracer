open Geometry

type t = {
    origine : float3;
    direction : float3;
    haut : float3;
    focale : float;
    aspect : float;
    largeur : float;
    champ_de_vision : float;
    pixels : int;
    anticrenelage : int
    }

let cree o d h a l c p ac =
    let ud = normalize d in
    { origine = o;
    direction = ud;
    haut = normalize (h -$ (ud *|* h) *% ud);
    focale = (l/.2.) /. tan (c /. 2.);
    aspect = a;
    largeur = l;
    champ_de_vision = c;
    pixels = p;
    anticrenelage = ac }

