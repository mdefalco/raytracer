open Geometry

type t = {
    aabb : Aabb.t;
    fonction : float3 -> float
}

let aabb imp = imp.aabb

exception Trouve of float
exception Stop
let intersection ray imp =
    match Aabb.intersection_double ray imp.aabb with
    None -> None
    | Some (tlow, thigh) ->
        let tlow = max tlow 0.0 in
        let eps = ref 1.0e-7 in
        let t = ref tlow in
        try
            for i = 0 to 200 do
                let i = Ray.impact ray !t in
                let distance = imp.fonction i in
                if !t > thigh
                then raise Stop;
                if distance <= !eps
                then raise (Trouve !t);
                t := !t +. distance;
                eps := max 1.0e-7 (!t *. echelle_pixel () *. 0.2)
            done;
            None
        with Trouve t -> Some t 
            | Stop -> None

let normale imp m =
    let delta = 1.0e-6 in
    normalize {
        x = imp.fonction (m +$ { x = delta; y = 0.0; z = 0.0 })
            -. imp.fonction (m +$ { x = -.delta; y = 0.0; z = 0.0 });
        y = imp.fonction (m +$ { y = delta; x = 0.0; z = 0.0 })
            -. imp.fonction (m +$ { y = -.delta; x = 0.0; z = 0.0 });
        z = imp.fonction (m +$ { z = delta; x = 0.0; y = 0.0 })
            -. imp.fonction (m +$ { z = -.delta; x = 0.0; y = 0.0 })
    }

let texcoords sph m = m
