open Geometry

type t = {
    centre : float3;
    rayon : float
    }

let intersection ray sph =
    let c = sph.centre in
    let a = ray.Ray.source in
    let d = ray.Ray.direction in
    let dx = a.x -. c.x in
    let dy = a.y -. c.y in
    let dz = a.z -. c.z in

    let alpha = d.x *. d.x +. d.y *. d.y +. d.z *. d.z in
    let beta = 2. *. (d.x *. dx +. d.y *. dy +. d.z *. dz) in
    let gamma = dx *. dx +. dy *. dy +. dz *. dz -. sph.rayon *. sph.rayon in
    let delta = beta *. beta -. 4. *. alpha *. gamma in
     if delta < 0.
     then None
     else
      let rdelta = sqrt delta in
      let t1 = (-.beta -. rdelta) /. (2. *. alpha) in
      let t2 = (-.beta +. rdelta) /. (2. *. alpha) in
      let t = if t1 < 0.0 then t2 else t1 in
      if t <= -0.01 
      then None
      else Some (max t 0.0)

let normale sph m = normalize (m -$ sph.centre)

let aabb sph =
    {
        Aabb.xlow = sph.centre.x -. sph.rayon;
        Aabb.xhigh = sph.centre.x +. sph.rayon;
        Aabb.ylow = sph.centre.y -. sph.rayon;
        Aabb.yhigh = sph.centre.y +. sph.rayon;
        Aabb.zlow = sph.centre.z -. sph.rayon;
        Aabb.zhigh = sph.centre.z +. sph.rayon
    }

let texcoords sph m = 
    let u = (1.0 /. sph.rayon) *% (m -$ sph.centre) in
    let phi = atan2 u.z u.x in
    let theta = acos u.y in
    { x = phi ; y = 0.0; z = theta }
