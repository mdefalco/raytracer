open Geometry
(* Axis-Aligned Bounding Box 
 *
 * un cas special de boites dont les plans
 * ont pour normales les axes du repere *)

type t = {
    xlow : float; xhigh : float;
    ylow : float; yhigh : float;
    zlow : float; zhigh : float
    }

let tranche ot xo xlow xhigh xd =
    match ot with
    None -> None
    | Some (tnear, tfar) ->
        if xd = 0.00
        then if xo < xlow || xo > xhigh
             then None
             else Some (tnear, tfar)
        else
            let t1 = (xlow-.xo) /. xd in
            let t2 = (xhigh-.xo) /. xd in
            let t1, t2 = if t1 > t2 then t2, t1
                                    else t1, t2 in
            let tnear = if t1 > tnear then t1 else tnear in
            let tfar = if t2 < tfar then t2 else tfar in

            if tnear > tfar || tfar < 0.0
            then None
            else Some (tnear, tfar)

let intersection_double ray aabb =
    let ot1 = tranche (Some (-1.0e100, 1.0e100))
        ray.Ray.source.x
        aabb.xlow aabb.xhigh 
        ray.Ray.direction.x in
    let ot2 = tranche ot1
        ray.Ray.source.y
        aabb.ylow aabb.yhigh 
        ray.Ray.direction.y in
    let ot3 = tranche ot2
        ray.Ray.source.z
        aabb.zlow aabb.zhigh 
        ray.Ray.direction.z in
    match ot3 with
    None -> None
    | Some (tnear,tfar) -> if tnear = -1.0e100 then None else Some (tnear,tfar)

let intersection ray aabb =
    match intersection_double ray aabb with
    None -> None
    | Some (tnear, _) -> Some (max tnear 0.0)

let normale aabb m = 
    if fabs (m.x -. aabb.xlow) < 0.5
    then { x = -1.0; y = 0.0; z = 0.0 }
    else if fabs (m.x -. aabb.xhigh) < 0.5
    then { x = 1.0; y = 0.0; z = 0.0 }
    else if fabs (m.y -. aabb.ylow) < 0.5
    then { x = 0.0; y = -1.0; z = 0.0 }
    else if fabs (m.y -. aabb.yhigh) < 0.5
    then { x = 0.0; y = 1.0; z = 0.0 }
    else if fabs (m.z -. aabb.zlow) < 0.5
    then { x = 0.0; y = 0.0; z = -1.0 }
    else { x = 0.0; y = 0.0; z = 1.0 }

let union aabb1 aabb2 =
    {
        xlow = min aabb1.xlow aabb2.xlow;
        xhigh = max aabb1.xhigh aabb2.xhigh;
        ylow = min aabb1.ylow aabb2.ylow;
        yhigh = max aabb1.yhigh aabb2.yhigh;
        zlow = min aabb1.zlow aabb2.zlow;
        zhigh = max aabb1.zhigh aabb2.zhigh
    }

let zero = { xlow = 0.0; xhigh = 0.0;
    ylow = 0.0; yhigh = 0.0;
    zlow = 0.0; zhigh = 0.0 }

let texcoords sph m = m
