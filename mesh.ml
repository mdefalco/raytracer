open Geometry
open Triangle 

type t = {
    origine : float3;
    taillex : float;
    taillez : float;
    pas : int;
    hauteur : float3 -> float;
    hmax : float
    }

let aabb m =
    {
        Aabb.xlow = m.origine.x;
        Aabb.ylow = m.origine.y;
        Aabb.zlow = m.origine.z;
        Aabb.xhigh = m.origine.x +. m.taillex;
        Aabb.yhigh = m.origine.y +. m.hmax;
        Aabb.zhigh = m.origine.z +. m.taillez;
    }

let texcoords msh m =
    {
        x = (m.x -. msh.origine.x) /. msh.taillex;
        y = 0.0;
        z = (m.z -. msh.origine.z) /. msh.taillez
    }

let sommet msh x z = {x=x; y=msh.hauteur {x=x;y=0.0;z=z}; z=z }

let sommet_grille msh i j =
    let fj = float_of_int j in
    let fi = float_of_int i in
    let fn = float_of_int msh.pas in
    
    let x = msh.origine.x +. msh.taillex *. (fi /. fn) in
    let z = msh.origine.z +. msh.taillez *. (fj /. fn) in

    sommet msh x z

let make_mesh msh t = 
let n = msh.pas in
let sommets = Array.make_matrix n n zero3 in

for j = 0 to n-1 do
for i = 0 to n-1 do
    sommets.(i).(j) <- sommet_grille msh i j
done;
done;
sommets

(* j'ai perdu du temps a comprendre que votre fonction etait fausse... *)
let minsomme a b =
match a with 
|None -> b 
| Some t -> match b with 
    |None -> Some t
    |Some p -> Some (min t p)


let intersection ray msh =
let sommets = make_mesh msh 1. in
let n = msh.pas in
let plume = ref None in 
    (* il y avait toujours une intersection avec votre -infini *)
for i = 0 to n-2 do
for j = 0 to n-2 do
    let plume2 = Triangle.intersection ray { p1 = sommets.(i).(j); p2 =
        sommets.(i+1).(j+1); p3 = sommets.(i+1).(j);
        n1 = zero3; n2 = zero3; n3 = zero3 } in
    let plume3 = Triangle.intersection ray {p1 = sommets.(i).(j); 
        p2 = sommets.(i).(j+1) ; p3 = sommets.(i+1).(j+1);
        n1 = zero3; n2 = zero3; n3 = zero3 } in
    plume := minsomme (minsomme (plume2) (plume3)) !plume;
done;
done; match !plume with
    None -> None
    | Some t -> if t <= -0.01 then None else Some (max t 0.0)

let normale msh m =
    let x = m.x in
    let z = m.z in
    let pasf= float_of_int msh.pas in
    (* on fait un petit element de surface de taille un dixieme du pas de grille *)
    let dx = 0.1 *. (msh.taillex /. pasf) in
    let dz = 0.1 *. (msh.taillez /. pasf) in

    let p1 = sommet msh x z in
    let p2 = sommet msh (x+.dx) z in
    let p3 = sommet msh x (z+.dz) in

    {x=0.0;y=1.0;z=0.0} (*normalize ( (p2 -$ p1) ^$ (p3 -$ p1) )*)

(* Non desole c'est vraiment trop du nimporte quoi pour moi ....
let normale msh m =
let sommets = make_mesh 200 msh.axes msh.hauteur 1. in
let (pas1,pas2) = msh.axes in
let {x=a;y=b;z=c} = pas2 in
let pas4 = {x=a;y= -.b;z=c} in
let {x=xm;y=ym;z=zm} = m in
let plume = ref {p1 = pas1; p2 = pas1; p3 = pas1;n1 = pas2;n2=pas2;n3=pas2} in
let n = Array.length sommets in
let dansface a b =
            let nf = a ^$ b in
            m *|* nf >= 0.0 in
for i = 0 to n-2 do
for j = 0 to n-2 do
    (* je corrige un peu mais je pense que c'est n'importe quoi *)
let plume2 = Triangle.intersection ray { p1 = sommets.(i).(j); p2 =
    sommets.(i+1).(j); p3 = sommets.(i+1).(j+1) } in
let plume3 = Triangle.intersection ray {p1 = sommets.(i).(j); 
    p2 = sommets.(i).(j+1) ; p3 = sommets.(i+1).(j+1) } in
 if dansface plume2.p1 plume2.p2
  && dansface plume2.p2 plume2.p3
   && dansface plume2.p3 plume2.p1 then plume:= plume2 else
 if dansface plume1.p1 plume1.p2
  && dansface plume1.p2 plume1.p3
   && dansface plume1.p3 plume1.p1 then plume:= plume1 done;done;
  (!plume.p3 -$ !plume.p1) ^$ (!plume.p2 -$ !plume.p1)
*)
   
(*Intersection: on parcourt tous les triangle et on fait le min des intersections*)
