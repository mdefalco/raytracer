open Geometry
open Lumiere

type bvh =
      Nil
    | Noeud of Aabb.t * bvh * bvh
    | Feuille of Aabb.t * Objet.t

let fx a = (a.Aabb.xlow +. a.Aabb.xhigh) /. 2.0
let fy a = (a.Aabb.ylow +. a.Aabb.yhigh) /. 2.0
let fz a = (a.Aabb.zlow +. a.Aabb.zhigh) /. 2.0

let cmpxyz f (a1,_) (a2,_) =
    let a1f, a2f = f a1, f a2 in
    if a1f < a2f
    then -1
    else if a1f > a2f
        then 1
        else 0

let global_aabb a =
    let b = ref (fst a.(0)) in
    for i = 1 to Array.length a - 1 do
        b := Aabb.union !b (fst a.(i))
    done;
    !b

let rec construit_bvh comp a =
    if Array.length a = 0
    then Nil
    else if Array.length a = 1
    then let b, o = a.(0) in Feuille (b,o)
    else let f = match comp with
            0 -> fx | 1 -> fy | _ -> fz in
        let ncomp = (comp + 1) mod 3 in
        Array.sort (cmpxyz f) a;
        let n1 = Array.length a / 2 in
        let n2 = Array.length a - n1 in
        let a1 = Array.sub a 0 n1 in
        let a2 = Array.sub a n1 n2 in
        let t1 = construit_bvh ncomp a1 in
        let t2 = construit_bvh ncomp a2 in
        Noeud (global_aabb a, t1, t2)

let rec intersecte_bvh ray bvh = match bvh with
    Feuille (a,o) -> begin
        match Aabb.intersection ray a with
        None -> Objet.Vide
        | Some t -> Objet.intersection ray o
    end
    | Noeud (a,t1,t2) -> begin
        match Aabb.intersection ray a with
        None -> Objet.Vide
        | Some t -> Objet.min_inter
                (intersecte_bvh ray t1)
                (intersecte_bvh ray t2) 
    end
    | Nil -> Objet.Vide

type options = {
    objet_id : bool;
    normale_map : bool;
    bvh_debug : int
    }

type t = {
    objets : Objet.t array;
    lumieres : lumiere array;
    camera : Camera.t;
    accelerateur: (Aabb.t * Objet.t) array * bvh * Objet.t array;
    options : options
    }

let cree options objets lumieres camera =
    let ob = Array.of_list ((List.map
        (fun l -> { Objet.primitive = Primitive.Lumiere l; Objet.matiere =
            Matiere.diffuse l.Lumiere.couleur }) lumieres) @
            objets) in

    let ab = Array.init (Array.length ob)
        (fun i -> Primitive.aabb ob.(i).Objet.primitive) in

    let non_bornes = ref 0 in

    for i = 0 to Array.length ab - 1 do
        if ab.(i) = None
        then incr non_bornes
    done;

    let nobjets = Array.length ob in
    let bornes = nobjets - !non_bornes in

    let tbornes  = Array.make bornes (Aabb.zero, ob.(0)) in
    let tnon_bornes = Array.make !non_bornes ob.(0) in 

    let cnb = ref 0 in
    let cb = ref 0 in

    for i = 0 to nobjets - 1 do
        match ab.(i) with
        None -> tnon_bornes.(!cnb) <- ob.(i);
            incr cnb
        | Some a -> 
            tbornes.(!cb) <- (a,ob.(i));
            incr cb
    done;

    let bvh = construit_bvh 0 tbornes in
       
    { 
        objets = ob;
        lumieres = Array.of_list lumieres;
        camera = camera;
        accelerateur = (tbornes, bvh, tnon_bornes);
        options = options
    }

let intersection ray scene =
    let mi = ref Objet.Vide in

    let tb, bvh, tnb = scene.accelerateur in

    for i = 0 to Array.length tnb - 1 do
        let ci = Objet.intersection ray tnb.(i) in
        mi := Objet.min_inter !mi ci
    done;

    let ci = intersecte_bvh ray bvh in
    mi := Objet.min_inter !mi ci;
    
    !mi

exception Trouve of int
let objet_id scene obj =
    try
        for i = 0 to Array.length scene.objets - 1 do
            if scene.objets.(i) = obj
            then raise (Trouve i)
        done;
        raise Not_found
    with Trouve i -> i
