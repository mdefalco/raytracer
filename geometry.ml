type float3 = {
    x : float;
    y : float;
    z : float;
}

let string_of_float3 { x = x; y = y; z = z } =
    "(" ^ string_of_float x ^ ", " ^ string_of_float y ^ ", " 
        ^ string_of_float z ^ ")"

let zero3 = { x = 0.0; y = 0.0; z = 0.0 }
let un3 = { x=1.0; y=1.0; z=1.0 }

let fabs f = if f >= 0.0 then f else -.f

let ( *$ ) v1 v2 = {
    x = v1.x *. v2.x ;
    y = v1.y *. v2.y ;
    z = v1.z *. v2.z
    }

let ( *|* ) v1 v2 = 
    let w = v1 *$ v2 in
    w.x +. w.y +. w.z

let ( -$ ) v1 v2 = {
    x = v1.x -. v2.x ;
    y = v1.y -. v2.y ;
    z = v1.z -. v2.z
    }

let ( +$ ) v1 v2 = {
    x = v1.x +. v2.x ;
    y = v1.y +. v2.y ;
    z = v1.z +. v2.z
    }

let ( *% ) l v = {
    x = l *. v.x;
    y = l *. v.y;
    z = l *. v.z
    }

let ( ^$ ) u v = {
    x = u.y *. v.z -. u.z *. v.y;
    y = u.z *. v.x -. u.x *. v.z;
    z = u.x *. v.y -. u.y *. v.x
    }

let normalize v =
    let n = sqrt (v *|* v) in
    if n > 0.0
    then (1.0/.n) *% v
    else { x=1.0; y=0.0; z=0.0 }

let dist2 a b = let v = (a-$b) in (v *|* v)
let norme v = sqrt (v *|* v)
let distance a b = norme (a -$ b)

let pi = acos (-1.0)

let interne_echelle_pixel = ref (1.0 /. 512.0)
let set_echelle_pixel w = interne_echelle_pixel := (1.0 /. float_of_int w)
let echelle_pixel ( )= !interne_echelle_pixel

let cree_base vec =
    let w = normalize vec in
    let u = if fabs w.x > fabs w.y
        then let invLen = 1.0 /. sqrt (w.x *. w.x +. w.z *. w.z) in
            { x = -.w.z *. invLen; y = 0.0; z = w.x *. invLen }
        else let invLen = 1.0 /. sqrt (w.y *. w.y +. w.z *. w.z) in
            { x = 0.0; y=w.z *. invLen; z = -.w.y *. invLen }
    in let v = w ^$ u in
    (u,v,w)

let comblin v (a,b,c) = (v.x *% a) +$ (v.y *% b) +$ (v.z *% c)
