open Geometry
open Objet
open Primitive
open Matiere
open Sphere
open BRDF

let rec radiance ray scene e1 =
	let i = Scene.intersection ray scene in
	match i with
       Vide -> (zero3)
	 | Point (t, obj) -> 
		let i = Ray.impact ray t in 
        let n = Objet.normale obj i in
		let n1 = if n *|* ray.Ray.direction <0. then n else zero3 -$ n in (*n_oriente*)
		let texcoords = Objet.texcoords obj i in
        let reflect = obj.matiere.reflectivite in	
		let obj_e = obj.matiere.ambiante in	
		let f = Matiere.evalue obj.matiere.diffuse texcoords in
		let p = (max (max f.x f.y) f.z) in
						
	(*let al = Random.float 1.0 in (*Russian Roulette*)
		if (ray.Ray.rebonds > 5 ) && ( al > (p) )
			then e1*%e	
			else let f1 =  1./.(p) *%f  in*)
	let f1 = f in
		(* if reflect = 1.0 (*diffuse = renvoie dans l'hémisphère*)
			then  *)
			(*for lumi = 0 to Array.length scene.Scene.lumieres - 1 do on considère la lumière ponctuelle comme la sphère lumineuse en fait il n'y a qu'une seule source lumineuse*)
				let shadow_ray,l,cos_a_max = ombre ray t lumisph i in
				let directlight = ref zero3 in
				let inter = Scene.intersection (shadow_ray) scene in (*on envoie des shadow_rays pour l'illumination directe*)
                (if ray.coord_ecran = (100,100)
                    then let Sphere {centre=c;rayon = r} = lumisph.primitive in (*la source lumineuse*)
                        Printf.printf
                        "%s %s %s %s\n" (string_of_float3 i)
                        (string_of_float3 c)
                        (string_of_float3 shadow_ray.Ray.direction) 
                        (string_of_float3 n1)
                )
                    ;

				begin match inter with
					|Point (t1,objet1) when objet1 = lumisph ->  
						let omega = 2.*.pi*.(1.-.cos_a_max) in 
                        if ray.coord_ecran = (100,100)
                        then Printf.printf "%f\n" omega;
                        (*
                        directlight:= !directlight +$ (1./.pi)*%( f1 *$ (
                             ((shadow_ray.Ray.direction )*|*n1)*.omega *%
                             lumisph.matiere.ambiante) ) (*1/pi pour le brdf*)
                             *)
                        let psn = shadow_ray.Ray.direction *|* n1 in
                        if psn > 0.0
                        then directlight := f1 *$ (psn *% lumisph.matiere.ambiante)  (*1/pi pour le brdf*)
					|_-> () end;
				let d = aleathemisphere n1 in
                if ray.coord_ecran = (100,100)
                then { x = 1.0; y=0.0; z = 0.0 }
                else let contrib = e1*%obj_e +$ !directlight in
                    let probCont = 0.5 in
                    let weight = if ray.Ray.rebonds <= 2 then 1.0 else
                        1.0/.probCont in
                    let rebond = 
                        if true && (ray.Ray.rebonds <= 2 || Random.float 1.0 >
                        probCont)
                        then weight *%
                            (radiance (Ray.relance ray (Ray.impact ray (t*.0.99)) d) scene 0.
                            *$ f1) 
                        else zero3 in
                    contrib +$ rebond
                    
                   (*
			else
		if reflect = 0.0 (*spéculaire = loi de descartes pour la reflection*)
			then
				let dir_refl = Ray.reflexion (-1.0 *% ray.Ray.direction) n in
				let ray_refl = Ray.relance ray (Ray.impact ray (t*.0.99)) dir_refl in
				  obj_e +$ ((radiance (ray_refl) scene 0.) *$ f1)
			else zero3 (*Pour l'instant je ne considère que de l'ideal diffuse ou ideal specular*)
            *)

		
			
let couleur_rayon ray scene =
(radiance ray scene  1.) 
