open Geometry

type lumiere = {
    position : float3;
    rayon : float;
    couleur : float3
    }

let echantillonne l p =
    (* Lumière ponctuelle diffuse avec echantillonage dans une sphère
       d'éclairage et de l'atténuation *)
    let vecPL = l.position -$ p in
    let u, v, w = cree_base vecPL in
    let cosThetaMax = 
        sqrt (max 0.0 (1.0 -. l.rayon *. l.rayon /. (vecPL *|* vecPL))) in
    let d = comblin (Echantillon.cone cosThetaMax) (u,v,w) in

    match Sphere.intersection (Ray.aux p d) 
        { Sphere.centre = l.position; Sphere.rayon = l.rayon } with
    | None -> failwith "Impossible."
    | Some t -> p +$ (t *% d), d, 1.0 /. (2.0 *. pi *. (1.0 -. cosThetaMax))

let to_sphere lum = { Sphere.centre = lum.position;
    Sphere.rayon = lum.rayon }


