open Geometry

exception Break
let champdedistance p m =
    let z = ref m in
    let dr = ref 1.0 in
    let r = ref 0.0 in
    let p = float_of_int p in

    let maxiter = 6 in
    let maxr = 4.0 in

try
    for i = 0 to maxiter do
        r := sqrt (!z *|* !z);

        if !r > maxr
        then raise Break;

        let theta = p *. acos (!z.z /. !r) in
        let phi = p *. atan2 !z.y !z.x in
        
        dr := !r ** (p -. 1.0) *. p *. !dr +. 1.0;

        let zr = !r ** p in

        z  := zr *%
            { x = sin theta *. cos phi;
              y = sin phi *. sin theta;
              z = cos theta } +$ m
    done;
    0.5 *. log !r *. !r /. !dr
with Break -> 0.5 *. log !r *. !r /. !dr
