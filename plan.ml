open Geometry

type t = {
    origine : float3;
    normale : float3
    }

let fabs f = if f >= 0.0 then f else -.f

let intersection ray plan =
    let o = plan.origine in
    let n = plan.normale in
    let a = ray.Ray.source in
    let d = ray.Ray.direction in

    let psn = (a -$ o) *|* n in
    let dn = d *|* n in

    if fabs dn < 0.05
    then None
    else let t = -. psn /. dn in
        if t <= 0.2 then None else Some (max 0.0 t)

let normale plan m = plan.normale

let texcoords sph m = m
