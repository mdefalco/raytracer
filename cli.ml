open Geometry
open Objet
open Scene
open Camera
open Ray 

let foi a = float_of_int a

let plot im x y s v =
    let w = Array.length im in
    let h = Array.length im.(0) in
    for i = 1 to s do
        for j = 1 to s do
            if x+i < w && y+j < h then im.(x+i).(y+j) <- v
        done
    done

let ligne im (x0,y0) (x1,y1) v =
    Printf.printf "line (%d,%d) -> (%d,%d)\n" x0 y0 x1 y1;
    let dx = abs (x1 - x0) in
    let dy = abs (y1 - y0) in
    let sx = if x0 < x1 then 1 else -1 in
    let sy = if y0 < y1 then 1 else -1 in

    let x = ref x0 in
    let y = ref y0 in
    let e = ref (dx - dy) in

    while !x <> x1 || !y <> y1 do
        plot im !x !y 3 v;
        let e2 = 2 * !e in
        if e2 > -dy then
            (e := !e - dy;
            x := !x + sx);
        if e2 < dx then
            (e := !e + dx;
            y := !y + sy)
    done

let calcule scene fo t cores pid = 
    let cam = scene.Scene.camera in
	
    let w = cam.Camera.pixels in

    let quant = w / cores in

    set_echelle_pixel w;
    let h = int_of_float ((float_of_int w)*. cam.Camera.aspect) in

    let ligne_scene a b im v =
        try
            let (ax,ay) = Rendu.optique_inv cam w h a in
            let (bx,by) = Rendu.optique_inv cam w h b in
            ligne im (ax,ay) (bx,by) v
        with Not_found -> ()
    in
        
    let image = Array.make_matrix w h { x = 0.0; y = 0.0; z = 0.0 } in

    let full_debug = ref [] in

    let nsamples = 1 +cam.Camera.anticrenelage in
    for sample = 0 to nsamples-1 do
        let starti = quant * ( (0 + pid) mod cores ) in
        for j = 0 to h-1 do
            for i = starti to starti+quant-1 do
                let r1 = 2.*.(Random.float 1.0)  in
                let dx = if r1<1. then sqrt(r1)-.1. else 1.-.sqrt(2.-.r1) in
                let r2 = 2.*.(Random.float 1.0)  in
                let dy = if r2<1. then sqrt(r2)-.1. else 1.-.sqrt(2.-.r2) in
                let ray_aa = Ray.optique cam w h i j  dx dy in
                let coul =  Rendu.couleur_rayon ray_aa scene in
                
                image.(i).(j) <- image.(i).(j) +$ coul
            done
        done;

        full_debug := !(Rendu.debug_path)::!full_debug;
        flush stdout;
        if sample mod 1 = 0
        then begin
            let npart = (pid + 0) mod cores in
            Printf.printf "%d-%d/%d\n" npart sample nsamples;
            let f = open_out_bin (Printf.sprintf "pics/%s__%03d_%02d.ppm" fo sample npart) in
            let image_aa = Array.make_matrix w h (0,0,0) in
            Printf.fprintf f "P6\n%d %d\n255\n" quant h;
            for j = 0 to h-1 do
                let j = h-1-j in
                for i = starti to starti+quant-1 do
                    let convert f = max 0 (min 255 (int_of_float (255. *. (f/. foi (1+sample))))) in
                    let coul = image.(i).(j) in
                    image_aa.(i).(j) <- ((convert coul.x), (convert coul.y), (convert coul.z))
                done
            done;

            (*
            List.iter (fun path ->
                List.iter (fun elt -> match elt with
                    Rendu.Impact (a,b) -> 
                        Printf.printf "%s -- %s\n" (string_of_float3 a) (string_of_float3 b);
                        ligne_scene a b image_aa (255,0,0)
                    | Rendu.Direction (a,b) -> ligne_scene a (a+$ (25.0 *% b))
                image_aa (0,255,0)) path) !full_debug;
            *)

            for j = 0 to h-1 do
                let j = h-1-j in
                for i = starti to starti+quant-1 do
                    let couleur_rayon r g b = output_char f (char_of_int r); 
                        output_char f (char_of_int g);
                        output_char f (char_of_int b) in
                    let (r,g,b) = image_aa.(i).(j) in
                    couleur_rayon r g b
                done
            done;
            close_out f
        end
    done


let load_file f =
    let ic = open_in f in
    let n = in_channel_length ic in
    let s = Bytes.create n in
    really_input ic s 0 n;
    close_in ic;
    Bytes.to_string s

let _ =
    let s = load_file Sys.argv.(1) in
    let t = 0.0 in
    let cores, pid = if Array.length Sys.argv = 5 then (int_of_string Sys.argv.(3), int_of_string Sys.argv.(4)) 
        else (1,0) in
    calcule (Sceneeval.from_string s t) Sys.argv.(2) t cores pid
