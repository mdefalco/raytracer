open Geometry

type t = {
    coin_inferieur_gauche : float3;
    vec_largeur : float3;
    vec_hauteur : float3
    }

let cree cam =
    let w = cam.Camera.direction ^$ cam.Camera.haut in
    let largeur = cam.Camera.largeur in
    let hauteur = cam.Camera.aspect *. largeur in
    {
    coin_inferieur_gauche = 
        cam.Camera.origine 
        +$ cam.Camera.focale *% cam.Camera.direction (* centre de la pellicule *)
        -$ (largeur/.2.) *% w
        -$ (hauteur /. 2.) *% cam.Camera.haut;
    vec_largeur = largeur *% w;
    vec_hauteur = hauteur *% cam.Camera.haut
    }

let pixel e w h i j =
 let ip = (i+.0.5)/.w in
 let jp = (j+.0.5)/.h in
 e.coin_inferieur_gauche
    +$ ip *% e.vec_largeur +$ jp *% e.vec_hauteur

