open Geometry

type t = {
    source : float3;
    direction : float3;
    coord_ecran : int * int;
    rebonds : int
    }

let optique cam w h i j di dj =
    let p = Pellicule.cree cam in
    let pi = Pellicule.pixel p 
        (float_of_int w) (float_of_int h)
        (di +. float_of_int i) (dj +. float_of_int j) in
    {
        source = cam.Camera.origine;
        direction = normalize (pi -$ cam.Camera.origine);
        coord_ecran = (i,j);
        rebonds = 0
    }

let impact ray t = ray.source +$ t *% ray.direction

let relance ray s d =
    { source = s; direction = d;
      coord_ecran = ray.coord_ecran;
      rebonds = ray.rebonds + 1 }

let reflexion u n = (2.0 *. (u *|* n)) *% n -$ u

let aux s d = { source = s; direction = d; coord_ecran = (-1,-1); rebonds = -1 }

