open Geometry

type t = {
    aabb : Aabb.t;
    fonction : float3 -> float
}

let aabb imp = imp.aabb

exception Trouve of float
let intersection ray imp =
    match Aabb.intersection_double ray imp.aabb with
    None -> None
    | Some (tlow, thigh) ->
        let tlow = max tlow 0.0 in
        let tinc = (thigh -. tlow) /. 100.0 in
        let t = ref tlow in
        let courant = ref (imp.fonction (Ray.impact ray tlow)) in
        try
            while !t < thigh do
                t := !t +. tinc;
                let i = Ray.impact ray !t in
                let nouveau = imp.fonction i in
                if !courant *. nouveau <= 0.00
                then raise (Trouve !t);
                courant := nouveau
            done;
            None
        with Trouve t -> Some t 

let normale imp m =
    let delta = 0.1 in
    normalize {
        x = imp.fonction (m +$ { x = delta; y = 0.0; z = 0.0 })
            -. imp.fonction (m +$ { x = -.delta; y = 0.0; z = 0.0 });
        y = imp.fonction (m +$ { y = delta; x = 0.0; z = 0.0 })
            -. imp.fonction (m +$ { y = -.delta; x = 0.0; z = 0.0 });
        z = imp.fonction (m +$ { z = delta; x = 0.0; y = 0.0 })
            -. imp.fonction (m +$ { z = -.delta; x = 0.0; y = 0.0 })
    }

let texcoords sph m = m
