open Geometry

let lerp3 t v1 v2 = (t *% v1) +$ ((1.0 -. t) *% v2)

let couleur ray l = 
    (* un simple calcul naif
     * on regarde l'angle que fait le rayon avec l'axe des y *)
    let theta = acos ray.Ray.direction.y in
    let t = (theta /. pi) ** 2.0 in

    lerp3 t { x = 0.8; y = 0.8; z = 0.8 }
        { x = 0.7; y = 0.8; z = 1.0 }

